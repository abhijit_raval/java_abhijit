import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class WordCount {
	private int charcterCount;
	private String character;
	
	public WordCount(int charcterCount, String character) {
		super();
		this.charcterCount = charcterCount;
		this.character = character;
	}
	public int getCharcterCount() {
		return charcterCount;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((character == null) ? 0 : character.hashCode());
		result = prime * result + charcterCount;
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WordCount other = (WordCount) obj;
		if (character == null) {
			if (other.character != null)
				return false;
		} else if (!character.equals(other.character))
			return false;
		if (charcterCount != other.charcterCount)
			return true;
		return false;
	}
	
	public void setCharcterCount(int charcterCount) {
		this.charcterCount = charcterCount;
	}
	public String getCharacter() {
		return character;
	}
	public void setCharacter(String character) {
		this.character = character;
	}
}
