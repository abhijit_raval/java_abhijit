import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class App {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter any String:");
		String character = sc.nextLine();
		char[] myString = character.toCharArray();
		List<WordCount> wcList = new ArrayList<WordCount>();

		for (char c : myString) {
			WordCount wc = new WordCount(0, String.valueOf(c));
			if (wcList.contains(wc)) {
				int i = wcList.indexOf(wc);
				wcList.get(i).setCharcterCount(wcList.get(i).getCharcterCount()+1);
			} else {
				wcList.add(new WordCount(1, String.valueOf(c)));
			}
		}
	//Ascending
	/*	Collections.sort(wcList, new Comparator<WordCount>() {

			@Override
			public int compare(WordCount o1, WordCount o2) {
				if (o1.getCharcterCount() == o2.getCharcterCount())
					return o1.getCharacter().compareTo(o2.getCharacter());
				return o1.getCharcterCount() > o2.getCharcterCount() ? 1 : -1;
			}
		});*/
		
		//Descending
		Collections.sort(wcList, new Comparator<WordCount>() {

			@Override
			public int compare(WordCount o1, WordCount o2) {
				if (o2.getCharcterCount() == o1.getCharcterCount())
					return o2.getCharacter().compareTo(o1.getCharacter());
				return o2.getCharcterCount() > o1.getCharcterCount() ? 1 : -1;
			}
		});
		
		for (WordCount count : wcList) {
			System.out.println(count.getCharacter() + " " + count.getCharcterCount());
		}
	}

}
